       IDENTIFICATION DIVISION.
       PROGRAM-ID. READ5L.
       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION.
       FILE-CONTROL. 
           SELECT 100-INPUT-FILE ASSIGN TO "TICKET.DAT"
              ORGANIZATION IS SEQUENTIAL
              FILE STATUS IS WS-INPUT-FILE-STATUS.
       DATA DIVISION.
       FILE SECTION.
       FD  100-INPUT-FILE
           BLOCK CONTAINS 0 RECORDS.
       01  100-INPUT-RECORD.
           05 TICK-DATE         PIC X(10).  
           05 TICK-TIME       PIC X(5). 
           05 TICE-GENDER  PIC 9(1). 
           05 TICKET-AGE   PIC X(3). 
           05 FILLLER           PIC X(62).



       WORKING-STORAGE SECTION.
       01  WS-INPUT-FILE-STATUS PIC X(2).
         88 FILE-OK      VALUE '00'.
         88 FILE-AT-END  VALUE '10'.
       01  WS-INPUT-COUNT             PIC 9(8) .
       
       PROCEDURE DIVISION.
       0000-MAIN-PROGRAM.
           PERFORM 1000-INITIAAL THRU 1000-EXIT
           
              PERFORM 2000-PROCESS THRU 2000-EXIT UNTIL FILE-AT-END
              

           PERFORM 3000-END THRU 3000-EXIT
           GOBACK.

       1000-INITIAAL.
           OPEN INPUT 100-INPUT-FILE 
           IF NOT FILE-OK
              DISPLAY "FILE NOT FOUND"
              STOP RUN
           END-IF
           PERFORM 8000-READ THRU 8000-EXIT.


       
       1000-EXIT.
           EXIT.

       2000-PROCESS.
           
           
           ADD 1 TO WS-INPUT-COUNT

           PERFORM 8000-READ THRU 8000-EXIT.
       2000-EXIT.
           EXIT.

       
           EXIT.
       2300-MOVE-TICKET.
           
       2300-EXIT.
           EXIT.
       

       3000-END.
           CLOSE 100-INPUT-FILE 
           
           DISPLAY "READ COUNT: " WS-INPUT-COUNT.
           
           
       3000-EXit.
           Exit.
       
       8000-READ.
           READ 100-INPUT-FILE.
       8000-EXIT.
           EXit.
